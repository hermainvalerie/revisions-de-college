---
author: Valérie Hermain
title: 🏡 Accueil
---

# Bienvenue dans le cours de Mrs Hermain

!!! info "Objectifs"

    😊 Ce site vous permettra d'actualiser vos connaissances de collège en anglais grâce à des vidéos et des fiches d'exercices et de vocabulaire au format pdf
